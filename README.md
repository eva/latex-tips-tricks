
This repository contains:

- Latex Tips and Tricks: useful_latex_packages_commands.tex

- A script to clean up bibtex source files: cleanbibtex.py
  To use, install the bibtexparser Python package:
    ```
    pip3 install bibtexparser
    ```

  Then run:
  ```
  python3 cleanbibtex.py bibfile.bib
  ```

  cleanbibtex.py will do the following:
    * check for duplicate entries (based on title)
    * remove a number of unneeded fields from each entry
    * normalize conference titles to look as e.g. "Computer Aided Verification (CAV)"
    * put the result in a file called bibfile_clean.bib
