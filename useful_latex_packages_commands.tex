
% ============================GENERAL TIPS======================================
% - One sentence per line or limit line length
%   (Makes it easier to follow changes in diff, and to find Latex bugs.)
%
% - Use macros judiciously 
%   (Too many macros slow down compilation and make it harder to read the source.)
%
% - Use empty lines for paragraph breaks instead of '\\'
%   (Makes the paragraphs visible in the source too.)
%
% - Use a tilde before cite and autoref commands: ~\cite{} 
%   (The tilde ~ is an unbreakable space, i.e. the line will never be broken at this position.)
%
% - Use \(...\) to write inline equations
%   ($...$ is a TeX command and gives more obscure error messages.)
%
% - Use \begin{equation*} or \begin{align*} to write centered equations.
%   ($$...$$ gives wrong spacing.)
%
% - add comments to the main file, e.g. why certain (especially unusual) packages
%   are included, or what certain commands do.
%   (Makes it easier to debug issues and fix formatting if needed.)
%
% - for conference papers, always download an up-to-date template
%   (Templates keep changing, and you want to avoid surprises at the last minute.)
%
% - Use a spell-checker. Many editors will have one built-in, so enable it.
%   (Saves your and your co-authors time fixing typos.)
%
% - Try to keep references consistent from the beginning or at least clean them up
%   before submission. For instance, copying bibtex entries from DBLP will give you
%   consistent formatting. Do NOT remove DOI entries - sometimes they are
%   mandatory but they are also good practice.
%   (The cleanbibtex.py script does some clean-up, specifically regarding conference names.)
%
% - Short Math Guide for Latex: http://tug.ctan.org/info/short-math-guide/short-math-guide.pdf
%   (Quick reference for math equations)
%
% - Latex Tables Generator: https://www.tablesgenerator.com/latex_tables
%   (Useful for generating tables with merged cells (going over several rows/columns))
%
% - Detexify: https://detexify.kirelabs.org/classify.html
%   (Draw the symbol you want, and the tool returns a list of possible Latex commands.)



% ==============================LISTINGS========================================

% Nice monospace font
\usepackage[T1]{fontenc}  % this is necessary for beramo to work
\usepackage[scaled=0.8]{beramono}  % adjust size

% Code listings
\usepackage{listings}
% useful listings commands
% \lstdefinelanguage{myCoolLang}{
%   basicstyle=\small\ttfamily,
%   mathescape=true,
%   morekeywords={fun, let, val, in, end, if, then, else},
%   morecomment=[l]{//},
%   morecomment=[s]{(*}{*)},
%   escapeinside={(*}{*)},  % e.g. if you want to add labels to lines: (*\label{aline}*)
%   commentstyle=\color{gray},
%   numbers=left,   % where to put the line numbers, choose 'none' to switch off
%   numberstyle=\tiny, % line number settings
%   stepnumber=2,
%   % for very customized style
%   literate=
%     {public}{{{\textbf{public}}}}{6}
%     {\\result}{{{\color{MidnightBlue}\@backslashchar{}result}}}{7}
% }
% \lstset{language=myCoolLang}


% ===============================TABLES=========================================
% Small Guide to Making Nice Tables:
% https://people.inf.ethz.ch/markusp/teaching/guides/guide-tables.pdf
% The gist: use the booktabs package, avoid vertical lines, minimum number of
% horizontal lines (e.g. \toprule, \midrule after header, \bottomrule),
% leave enough space between rows, by default align left
\usepackage{booktabs}

% increase spacing between rows (can also be adjusted on a table-by-table basis)
\renewcommand{\arraystretch}{1.2}

% Wrap a table to fit to page width: \begin{adjustbox}{width=1\textwidth}
\usepackage{adjustbox}

% Highlight rows/cells by background color: \rowcolor{black!5}/\cellcolor{}
% Don't go overboard with colors, less is more.
\usepackage{colortbl}

% To reduce font size of tables in Springer LLCS 
% \fontsize{8}{10}\selectfont  % adjust size to what you need

% ===============================FLOATS=========================================
% Latex' float placement defaults are not always the best or space-efficient, 
% but can be adjusted. Float pages contain only floats and no text.
\renewcommand{\topfraction}{.98} % max fraction of floats at top
\renewcommand{\textfraction}{.02} % allow minimal text w. figs
\renewcommand{\bottomfraction}{.7} % max fraction of floats at bottom
\renewcommand{\floatpagefraction}{.66} % require fuller float pages
% N.B.: floatpagefraction MUST be less than topfraction!
\renewcommand{\dbltopfraction}{.98} % fit big float above 2-col. text
\renewcommand{\dblfloatpagefraction}{.66} % require fuller float pages

% In general, place floats at the top of the page ([t]). This uses less space
% and does not break up the text.

% If you have trouble placing floats where you want them, it is helpful to
% remember Latex' algorithm for placing them. In short, Latex will attempt to
% place a float as soon as it encounters it in the source. If the current page
% is already too full, the float moves to the next page. A very complete
% explanation can be found here:
% https://www.tug.org/TUGboat/tb35-3/tb111mitt-float.pdf

% ======================OTHER USEFUL PACKAGES/COMMANDS==========================
% Flexible TODOs
\usepackage{todonotes}
% you can use different colors for different people:
\newcommand*{\name}[1]{\todo[author=INITIALS,caption={},color=DarkOrchid!60,inline,backgroundcolor=Apricot]{#1}}


% Use \autoref instead of \ref for references. It automatically adds the correct
% prefix (e.g. Section 1, Figure 2, ...), and the prefix will also be part of the
% clickable reference, not only the number. 
\usepackage{hyperref}


% Adjust section names in references (you can also adjust names of other items)
\def\sectionautorefname{Section} % capitalize Section
\def\subsectionautorefname{Section} % 'Section 5.4' instead of 'Subsection 5.4'
\def\subsubsectionautorefname{Section}

% tighten paragraphs that are like one word over, after a paragraph, put \TightenPar{1}
\newcommand{\TightenPar}[1]{\looseness=-#1}

% to avoid overfull boxes, i.e. words that go outside of the margins, use only locally
\begin{sloppypar}
  % your paragraph here
\end{sloppypar}

% makes the text 'magically' look nicer
\usepackage[activate={true,nocompatibility},final,tracking=true,kerning=true,spacing=true,factor=stretch=10,shrink=10]{microtype}

% Show document margins 
\usepackage{showframe}


% For dummy text
\usepackage{lipsum}


% For placing captions to the right (or left) of a figure/table, instead of top and bottom
\usepackage{sidecap}
% \begin{SCfigure}, \end{SCfigure}


% For drawing diagrams, graphs, etc. directly in Latex
\usepackage{tikz}

% Drawing a figure with tikz can potentially take unbounded time and may make
% it difficult to edit the figure for your collaborators. Alternatively, you can
% do your plots in python (matplotlib) and save them at a high quality:
% f, ax = plt.subplots()
% # do your plotting here
% f.savefig("myplot.pdf", dpi=300, bbox_inches='tight')
% Ideally, always store the python plotting script in the paper repo.

% for allowing URL line breaks (to check!)
\usepackage{url}
\makeatletter
\g@addto@macro{\UrlBreaks}{\UrlOrds}
\makeatother
