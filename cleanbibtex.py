import os
import sys
from copy import copy

import bibtexparser
from bibtexparser.bwriter import BibTexWriter

if len(sys.argv) < 2:
    print("Not enough arguments\n Usage: python3 cleanbibtex.py source-bib.bib")
    exit(-1)

source = sys.argv[1]
target = source.replace('.bib', '_clean.bib')

with open(source) as bibtex_file:
    parser = bibtexparser.bparser.BibTexParser(ignore_nonstandard_types=False, interpolate_strings=False)
    bib_database = bibtexparser.load(bibtex_file, parser)


unnecessaryFields = ['doi', 'bdsk-url-1', 'keywords', 'date-added', 'date-modified',
    'numpages', 'bdsk-url-2', 'month', 'address', 'timestamp', 'biburl', 'bibsource', 'editor']

unnecessaryFieldsProceedings = ['pages', 'volume', 'publisher', 'url']

unnecessaryFieldsArticle = ['pages', 'publisher', 'url']

conferences = {
  'CAV': 'Computer Aided Verification',
  'FMCAD': 'Formal Methods in Computer Aided Design',
  'VMCAI': 'Verification, Model Checking, and Abstract Interpretation',
  'TACAS': 'Tools and Algorithms for the Construction and Analysis of Systems',
  'ESOP': 'Programming Languages and Systems',
  'OOPSLA': 'Object Oriented Programming Systems Languages {\&} Applications',
  'Formal Methods in Computer Aided Design': 'FMCAD',
  'IJCAR': 'International Joint Conference on Automated Reasoning',
  'ESEC/FSE': 'Foundations of Software Engineering',
  'FSE': 'Foundations of Software Engineering',
  'ATVA': 'Automated Technology for Verification and Analysis',
  'SAS': 'Static Analysis Symposium',
  'PLDI': 'Programming Language Design and Implementation',
  'ICLR': 'International Conference on Learning Representations',
  'NeurIPS': 'Neural Information Processing Systems',
  'HSCC': 'Hybrid Systems: Computation and Control',
  'ICAPS': 'International Conference on Automated Planning and Scheduling',
  'IJCAI': 'International Joint Conference on Artificial Intelligence',
  'AAMAS': 'Autonomous Agents and Multi-Agent Systems',
  'ACL': 'Annual Meeting of the Association for Computational Linguistics',
  'AAAI': 'Conference on Artificial Intelligence',
  'ICRA': 'International Conference on Robotics and Automation',
  'POPL': 'Principles of Programming Languages',
  'ICCPS': 'International Conference on Cyber-Physical Systems',
  'ICSE': 'International Conference on Software Engineering',
  'ICML': 'International Conference on Machine Learning'
}

allTitles = []


for ref in bib_database.entries:
    # check for duplicate titles
    if ref['title'].upper() in allTitles:
        print(f"Warning: duplicate entries with title: {ref['title']}")
    else:
        allTitles.append(ref['title'].upper())   # if the title is unique cache it

    # remove unnecessary fields (for all entry types)
    for field in unnecessaryFields:
        if field in ref:
            ref.pop(field)

    # for proceedings only
    if ref['ENTRYTYPE'] == 'inproceedings':
        for field in unnecessaryFieldsProceedings:
            if field in ref:
                ref.pop(field)

    # for articles
    if ref['ENTRYTYPE'] == 'article':
        for field in unnecessaryFieldsArticle:
            if field in ref:
                ref.pop(field)

    # normalize conference titles
    if ref['ENTRYTYPE'] == 'inproceedings':
        booktitle = ref['booktitle'].lstrip('{').rstrip('}')
        #print(booktitle)

        for confAbbr, confName in conferences.items():
          if confName in booktitle and confAbbr in booktitle:
            ref['booktitle'] = '%s (%s)' % (confName, confAbbr)

          if booktitle == confAbbr:
            #print('true')
            ref['booktitle'] = '%s (%s)' % (confName, confAbbr)


    # to reduce the diff put the keys back to capital letters
    oldkeys = copy(list(ref.keys()))
    for k in oldkeys:
        if k == 'ENTRYTYPE' or k == 'ID':
            continue
        if k.capitalize() == k:
            continue
        if '-' in k:
            updK = '-'.join([pt.capitalize() for pt in k.split('-')])
            if updK == k:
                continue
        else:
            updK = k.capitalize()
        ref[updK] = ref[k]
        ref.pop(k)


writer = BibTexWriter()
writer.indent = '  '
writer.order_entries_by = None    # print entries in the same order as before
bibtex_str = bibtexparser.dumps(bib_database, writer)
output_file = open(target, "w")
output_file.write(bibtex_str)
output_file.close()
